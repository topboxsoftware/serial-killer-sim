﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainButtonBehavior : MonoBehaviour {

	//Maybe consider cleaning up this code by recognizing that all button starting positions are equal to startFirstButton.
	//Fix: On smaller viewport sizes, there are panels visible that should not be. Information panel, for example. (Done)

	bool myAction;
	bool menuActive;
	int actionState2;
	Quaternion myQ;
	
	Vector3 finishFirstButton;
	Vector3 startFirstButton;
	Vector3 finishFirstButtonChild;
	Vector3 finishFirstButtonChildTwo;

	Vector3 finishSecondButton;
	Vector3 startSecondButton;
	
	Vector3 finishThirdButton;
	Vector3 finishThirdButtonChild;
	
	Vector3 startScrollPanel;
	Vector3 finishScrollPanel;
	
	Vector3 startExtras;
	Vector3 finishExtras;
	
	Vector3 finishThirdButtonChildTwo;

	float weight;
	float weight2;
	float weight3;
	float weight4;
	
	float speed;
	float mRotation;

	int actionState;

	public Button firstButton;
	public Button secondButton;
	public Button firstButtonChild;
	public Button firstButtonChildTwo;
	public Button thirdButton;
	public Button thirdButtonChild;
	public Button mainButton;
	public Button thirdButtonChildTwo;
	
	public GameObject myPanel;
	public GameObject commandPanel;
	
	public Text dataText;
	public Text dataText2;
	public Text ageText;
	//public Text genderText;
	public Text visibilityText;
	public Text jobText;
	//public Text familyText;
	public Text friendText;
	
	public Text firstNameText;
	public Text secondNameText;
	public Text thirdNameText;
	public Text TargetText;
	
	public Text researchText;
	public Text bribesText;
	public Text offenseText;
	public Text policeText;
	
	public Text revealText;
	
	public GameObject extras;
	
	public GameObject informationPanel;
	public GameObject victimPanel;
	
	public GameObject timeStatsPanel;
	public GameObject newsPanel;
	
	public GameObject scrollPanel;
	public GameObject myArrow;
	
	public Text targetText;
	
	// Use this for initialization
	void Start () 
	{
		//Delete the below line to save the generated data
		PlayerPrefs.DeleteAll();
		//PlayerPrefs.SetString("Target", "");
		
		//PlayerPrefs.SetString("Testing", "Test!");
		//PlayerPrefs.SetString("Target", "No Target");
		
		myAction = false;
		actionState = 0;
		weight = 0;
		weight2 = 0;
		weight3 = 0;
		speed = 5F;
		menuActive = true;
		actionState2 = 0;
		myQ = Quaternion.identity;
		mRotation = 0F;
		
		startFirstButton = new Vector3 (firstButton.transform.position.x, firstButton.transform.position.y, firstButton.transform.position.z);
		startSecondButton = new Vector3 (secondButton.transform.position.x, secondButton.transform.position.y, secondButton.transform.position.z);

		finishFirstButton = new Vector3 (startFirstButton.x, startFirstButton.y - firstButton.GetComponent<RectTransform>().rect.height * .96F, finishFirstButton.z);
		finishSecondButton = new Vector3 (startSecondButton.x, startSecondButton.y - secondButton.GetComponent<RectTransform>().rect.height * .96F * 2, finishSecondButton.z);

		finishFirstButtonChild = new Vector3 (firstButtonChild.transform.position.x + firstButtonChild.GetComponent<RectTransform> ().rect.width * .99F, finishFirstButton.y, firstButtonChild.transform.position.z);
		finishFirstButtonChildTwo = new Vector3 (firstButtonChildTwo.transform.position.x + firstButtonChild.GetComponent<RectTransform> ().rect.width * .99F, finishSecondButton.y, firstButtonChildTwo.transform.position.z);
		finishThirdButton = new Vector3(thirdButton.transform.position.x, thirdButton.transform.position.y - thirdButton.GetComponent<RectTransform>().rect.height * .96F * 3F, thirdButton.transform.position.z);
		finishThirdButtonChild = new Vector3(thirdButtonChild.transform.position.x + thirdButtonChild.GetComponent<RectTransform>().rect.width * .99F, finishSecondButton.y, finishThirdButton.z);
		finishThirdButtonChildTwo = new Vector3(thirdButtonChild.transform.position.x + thirdButtonChildTwo.GetComponent<RectTransform>().rect.width * .99F, finishThirdButton.y, finishThirdButton.z);
		startScrollPanel = new Vector3(scrollPanel.transform.position.x, scrollPanel.transform.position.y, scrollPanel.transform.position.z);
		finishScrollPanel = new Vector3(scrollPanel.transform.position.x, scrollPanel.transform.position.y - scrollPanel.GetComponent<RectTransform>().rect.height, scrollPanel.transform.position.z);
		
		startExtras = new Vector3(extras.transform.position.x, extras.transform.position.y, extras.transform.position.z);
		finishExtras = new Vector3(extras.transform.position.x, extras.transform.position.y + extras.GetComponent<RectTransform>().rect.height, extras.transform.position.z);
		
		
		//startScrollPanel = new Vector3(scrollPanel.transform.position.x, scrollPanel.position.y, scrollPanel.position.z);
		//endScrollPanel = new Vector3(scrollPanel.transform.position.x, scrollPanel.transform.position.y + scrollPanel.GetComponent<RectTransform>().rect.height, scrollPanel.transform.position.z);
	}
	
	// Update is called once per frame

	void Update() 
	{
		
		//Delete this statement before deployment
		
		if(Input.GetKeyUp(KeyCode.Space))
		{
			Application.LoadLevel(Application.loadedLevel);
		}
		
		if (myAction) 
		{
			weight = weight + Time.deltaTime * speed;

			firstButton.transform.position = Vector3.Lerp (startFirstButton, finishFirstButton, weight);
			firstButtonChild.transform.position = Vector3.Lerp (startFirstButton, finishFirstButton, weight);
			firstButtonChildTwo.transform.position = Vector3.Lerp (startFirstButton, finishSecondButton, weight);
			
			secondButton.transform.position = Vector3.Lerp (startSecondButton, finishSecondButton, weight);
			thirdButton.transform.position = Vector3.Lerp(startFirstButton, finishThirdButton, weight);
			thirdButtonChild.transform.position = Vector3.Lerp(startFirstButton, finishThirdButton, weight);
			thirdButtonChildTwo.transform.position = Vector3.Lerp(startFirstButton, finishThirdButton, weight);
		}

		if (actionState == 1) 
		{
		
			weight2 = weight2 + Time.deltaTime * speed;
			firstButtonChild.transform.position = Vector3.Lerp(finishFirstButton, finishFirstButtonChild, weight2);
			firstButtonChildTwo.transform.position = Vector3.Lerp(finishSecondButton, finishFirstButtonChildTwo, weight2);
		}
		
		if(actionState == 2)
		{
			weight2 = weight2 + Time.deltaTime * speed;
			thirdButtonChild.transform.position = Vector3.Lerp(finishSecondButton, finishThirdButtonChild, weight2);
			thirdButtonChildTwo.transform.position = Vector3.Lerp(finishThirdButton, finishThirdButtonChildTwo, weight2);
		}


		//Reverse scroll 
		if(actionState2 == 1)
		{
			weight4 = weight4 + Time.deltaTime * speed;
			scrollPanel.transform.position = Vector3.Lerp(startScrollPanel, finishScrollPanel, weight4);
			myArrow.transform.rotation = Quaternion.RotateTowards(myArrow.transform.rotation, myQ, 17);
			
			extras.transform.position = Vector3.Lerp(startExtras, finishExtras, weight4);
		}else if(actionState2 == 2)
		{
			weight4 = weight4 + Time.deltaTime * speed;
			scrollPanel.transform.position = Vector3.Lerp(finishScrollPanel, startScrollPanel, weight4);
			myArrow.transform.rotation = Quaternion.RotateTowards(myArrow.transform.rotation, myQ, 17);
			
			extras.transform.position = Vector3.Lerp(finishExtras, startExtras, weight4);
		}
		
		


	}

	public void victimChild()
	{
		if (actionState != 1) 
		{
			weight2 = 0;
			actionState = 1;
		} 
		else 
		{
			actionState = 0;
			firstButtonChild.transform.position = finishFirstButton;
			firstButtonChildTwo.transform.position = finishSecondButton;
		}
	}
	
	public void actionChild()
	{
		if(actionState != 2)
		{
			weight2 = 0;
			actionState = 2;
		}
		else
		{
			actionState = 0;
			thirdButtonChild.transform.position = finishThirdButton;
		}
	}
	
	public void victimSelect()
	{
		
		//mainPanel.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,0,0);
		if(actionState != 3)
		{
			//Debug.Log("yes");
			actionState = 3;
			myPanel.SetActive(true);
			mRotation = 0F;
			myQSet();
			if(PlayerPrefs.GetString("Target") != "No Target")
			{
				
				//Populate current target information
				if(PlayerPrefs.GetString("Target") == "Person1")
				{
					//Populate this with relevant stalking data
					dataText2.text = PlayerPrefs.GetString("FirstName1") + " " + PlayerPrefs.GetString("LastName1");
					
					researchText.text = "Research: " + PlayerPrefs.GetInt("CurrentResearch");
					bribesText.text = "Money: $" + PlayerPrefs.GetInt("CurrentBribes");
					offenseText.text = "Weapons: " + PlayerPrefs.GetInt("CurrentWeapons");
					policeText.text = "Police Awareness: " + PlayerPrefs.GetInt("CurrentPolice"); 
					
					
				}
			}
			
			resetPos();
		}
		
		menuOption();
	}
	
	void resetPos()
	{
		myAction = false;
		firstButton.transform.position = startFirstButton;
			firstButtonChild.transform.position = startFirstButton;
			firstButtonChildTwo.transform.position = startFirstButton;
			secondButton.transform.position = startSecondButton;
			thirdButton.transform.position = startFirstButton;
			thirdButtonChild.transform.position = startFirstButton;
			thirdButtonChildTwo.transform.position = startFirstButton;
			myAction = false;
			weight = 0;
			weight2 = 0;
			actionState = 0;
	}
	
	void generateNameData()
	{
		
		//Generating Names
		newName("FirstName1", "LastName1");
		newName("FirstName2", "LastName2");
		newName("FirstName3", "LastName3");
		
		//Generating Ages
		newAge("Age1");
		newAge("Age2");
		newAge("Age3");
		
		//Generating Jobs
		newJob("Job1");
		newJob("Job2");
		newJob("Job3");
		
		newVisibility("Visibility1");
		newVisibility("Visibility2");
		newVisibility("Visibility3");
		
		newFriends("Friends1");
		newFriends("Friends2");
		newFriends("Friends3");
		
		//Debug.Log(PlayerPrefs.GetInt("Age1"));
	}
	
	void newFriends(string friends)
	{
		//Consider typing certain friend counts to job + age
		int x = (int)Random.Range(0F, 15F);
		PlayerPrefs.SetInt(friends, x);
	}
	
	void newAge(string age)
	{
		int x = (int)Random.Range(16F, 85F);
		PlayerPrefs.SetInt(age, x);
	}
	
	void newVisibility(string visibility)
	{
		//Considering tying certain visibilities jobs
		int x = (int)Random.Range(1F, 10F);
		PlayerPrefs.SetInt(visibility, x);
	}
	
	void newJob(string job)
	{
		int x = (int)Random.Range(1F, 11F);
		
		if(x == 1)
		{
			PlayerPrefs.SetString(job, "Doctor");
		}
		else if(x == 2)
		{
			PlayerPrefs.SetString(job, "Author");
		}
		else if(x == 3)
		{
			PlayerPrefs.SetString(job, "Unemployed");
		}
		else if(x == 4)
		{
			PlayerPrefs.SetString(job, "Teacher");
		}
		else if(x == 5)
		{
			PlayerPrefs.SetString(job, "Counselor");
		}
		else if(x == 6)
		{
			PlayerPrefs.SetString(job, "Farmer");
		}
		else if(x ==7)
		{
			PlayerPrefs.SetString(job, "Banker");
		}
		else if(x == 8)
		{
			PlayerPrefs.SetString(job, "Clerk");
		}
		else if(x == 9)
		{
			PlayerPrefs.SetString(job, "Janitor");
		}
		else if(x == 10)
		{
			PlayerPrefs.SetString(job, "Chef");
		}
		else if(x ==11)
		{
			PlayerPrefs.SetString(job, "Hitman");
		}
	}
	
	void newName(string first, string  last)
	{
		//Generates and assigns a new name
		int x = (int)Random.Range(1F, 10F);
		int y = (int)Random.Range(1F, 10F);
		
		if(x == 1)
		{
			PlayerPrefs.SetString(first, "Josh");
		}
		else if(x == 2)
		{
			PlayerPrefs.SetString(first, "Sammy");
		}
		else if(x == 3)
		{
			PlayerPrefs.SetString(first, "Tammy");
		}
		else if(x == 4)
		{
			PlayerPrefs.SetString(first, "Sarah");
		}
		else if(x == 5)
		{
			PlayerPrefs.SetString(first, "Chris");
		}
		else if(x == 6)
		{
			PlayerPrefs.SetString(first, "Fiona");
		}
		else if(x == 7)
		{
			PlayerPrefs.SetString(first, "Jim");
		}
		else if(x == 8)
		{
			PlayerPrefs.SetString(first, "Molly");
		}
		else if(x == 9)
		{
			PlayerPrefs.SetString(first, "Kristin");
		}
		else if(x == 10)
		{
			PlayerPrefs.SetString(first, "Erika");
		}
		
		if(y == 1)
		{
			PlayerPrefs.SetString(last, "Williams");
		}
		else if(y == 2)
		{
			PlayerPrefs.SetString(last, "Jones");
		}
		else if(y == 3)
		{
			PlayerPrefs.SetString(last, "Bloodworth");
		}
		else if(y == 4)
		{
			PlayerPrefs.SetString(last, "Wilson");
		}
		else if(y == 5)
		{
			PlayerPrefs.SetString(last, "Kim");
		}
		else if(y == 6)
		{
			PlayerPrefs.SetString(last, "Lee");
		}
		else if(y == 7)
		{
			PlayerPrefs.SetString(last, "Smith");
		}
		else if(y == 8)
		{
			PlayerPrefs.SetString(last, "Jordan");
		}
		else if(y == 9)
		{
			PlayerPrefs.SetString(last, "Taparia");
		}
		else if(y == 10)
		{
			PlayerPrefs.SetString(last, "Wang");
		}
		
	}
	
	void menuOpened()
	{
		//Issue: Arrow doesn't scroll back.
		
		if(PlayerPrefs.GetInt("MenuOpenedEver") != 1)
		{
			PlayerPrefs.SetInt("MenuOpenedEver", 1);
			PlayerPrefs.SetString("Target", "No Target");
			generateNameData();
		}
		
		
		if(actionState2 == 1)
		{
			weight4 = 0;
			mRotation = 0F;	
			actionState2 = 2;
			Debug.Log(mRotation);
					
		}
		
		firstNameText.text = PlayerPrefs.GetString("FirstName1") + " " + PlayerPrefs.GetString("LastName1");
		secondNameText.text = PlayerPrefs.GetString("FirstName2") + " " + PlayerPrefs.GetString("LastName2");
		thirdNameText.text = PlayerPrefs.GetString("FirstName3") + " " + PlayerPrefs.GetString("LastName3");
	
	}
	
	public void victimSelect2()
	{
			
			menuOpened();
			victimSelect();
		
	}
	
	public void onClick()
	{
		
		menuOpened();
		
		if (!myAction) 
		{
			myAction = true;
		}
		else 
		{
			firstButton.transform.position = startFirstButton;
			firstButtonChild.transform.position = startFirstButton;
			firstButtonChildTwo.transform.position = startFirstButton;
			secondButton.transform.position = startSecondButton;
			thirdButton.transform.position = startFirstButton;
			thirdButtonChild.transform.position = startFirstButton;
			thirdButtonChildTwo.transform.position = startFirstButton;
			myAction = false;
			weight = 0;
			weight2 = 0;
			actionState = 0;
		}
	}
	
	public void revealMore()
	{
		if(TargetText.text != "")
		{
			informationPanel.SetActive(true);
		}
	}
	
	public void stalkTarget()
	{
		if(PlayerPrefs.GetString("Target") != "No Target")
		{
			GameObject.Find("FadePanel").GetComponent<ScreenFade>().fadeOut();
		}
	}
	
	public void selectName(string name)
	{
		
		if(name == "firstName")
		{
			TargetText.text = "Person1";
				
			dataText.text = PlayerPrefs.GetString("FirstName1") + " " + PlayerPrefs.GetString("LastName1");
			dataText2.text = PlayerPrefs.GetString("FirstName1") + " " + PlayerPrefs.GetString("LastName1");
		
			ageText.text = "Age: " + PlayerPrefs.GetInt("Age1");
			jobText.text = "Job: " + PlayerPrefs.GetString("Job1");
			
			visibilityText.text = "Visibility: " + PlayerPrefs.GetInt("Visibility1");	
			friendText.text = "Friends: " + PlayerPrefs.GetInt("Friends1");
		}
		else if(name == "secondName")
		{
			TargetText.text = "Person2";
			
			dataText.text = PlayerPrefs.GetString("FirstName2") + " " + PlayerPrefs.GetString("LastName2");
			dataText2.text = PlayerPrefs.GetString("FirstName2") + " " + PlayerPrefs.GetString("LastName2");
			
			ageText.text = "Age: " + PlayerPrefs.GetInt("Age2");
			jobText.text = "Job: " + PlayerPrefs.GetString("Job2");
			
			visibilityText.text = "Visibility: " + PlayerPrefs.GetInt("Visibility2");
			friendText.text = "Friends: " + PlayerPrefs.GetInt("Friends2");
		}
		else if(name == "thirdName")
		{
			TargetText.text = "Person3";
			
			dataText.text = PlayerPrefs.GetString("FirstName3") + " " + PlayerPrefs.GetString("LastName3");
			dataText2.text = PlayerPrefs.GetString("FirstName3") + " " + PlayerPrefs.GetString("LastName3");
			
			ageText.text = "Age: " + PlayerPrefs.GetInt("Age3");
			jobText.text = "Job: " + PlayerPrefs.GetString("Job3");
			
			visibilityText.text = "Visibility: " + PlayerPrefs.GetInt("Visibility3");
			friendText.text = "Friends: " + PlayerPrefs.GetInt("Friends3");
		}
	}
	
	public void setTarget()
	{
		
		//Set current target
		PlayerPrefs.SetString("Target", TargetText.text);
		
		//Set useable values
		PlayerPrefs.SetInt("CurrentResearch", 0);
		PlayerPrefs.SetInt("CurrentBribes", 0);
		PlayerPrefs.SetInt("CurrentWeapons", 0);
		PlayerPrefs.SetInt("CurrentPolice", 0);
		
		myPanel.SetActive(false);
		menuOption();
	}
	
	string whoIsTarget()
	{
		if(PlayerPrefs.GetString("Target") != "No Target")
		{
			if(TargetText.text == "Person1")
			{
				return PlayerPrefs.GetString("FirstName1") + " " + PlayerPrefs.GetString("LastName1");
			}
			else if(TargetText.text == "Person2")
			{
				return PlayerPrefs.GetString("FirstName2") + " " + PlayerPrefs.GetString("LastName2");
			}
			else if(TargetText.text == "Person3")
			{
				return PlayerPrefs.GetString("FirstName3") + " " + PlayerPrefs.GetString("LastName3");
			}
			else
			{
				return "No Target";
			}
		}
		else
		{
			return "No Target";
		}
	}
	
	void myQSet()
	{
		myQ = Quaternion.Euler(0F, 0F, mRotation);
	}
	
	public void scrollDown()
	{
		
		if(menuActive)
		{
			if(PlayerPrefs.GetString("Target") != "No Target")
			{
				targetText.text = whoIsTarget();
				revealText.text = "More Information";
			}
			else 
			{
				targetText.text = "No Target";
				revealText.text = "Select Target";	
			}
			
			weight4 = 0;
			
			if(mRotation == 0F)
			{
				mRotation += 180F;
			}
			else if(mRotation == 180F)
			{
				mRotation -= 180F;
			}
			//Debug.Log(mRotation);
			
			myQSet();
			
			
			
			//mRotation = mRotation  180F;
			//myQ = Quaternion.Euler(0F, 0F, mRotation);
			
			if(actionState2 == 0)
			{
				actionState2 = 1;
			}else if(actionState2 == 1)
			{
				actionState2 = 2;
			}else if(actionState2 == 2)
			{
				actionState2 = 1;
			}
			
			
			//Debug.Log(actionState2);
			//Debug.Log("te");
			/*
			if(!scrollActive)
			{
				scrollActive = true;
			}
			else
			{
				scrollActive = false;
			}
			*/
		}
	}
	
	public void killAttempt()
	{
		
	}
	
	public void giveUp()
	{
		PlayerPrefs.SetString("Target", "No Target");
		victimPanel.SetActive(false);
		menuOption();
	}
	
	public void studyTarget()
	{
		if(PlayerPrefs.GetString("Target") != "No Target")
		{
			//myPanel.SetActive(true);
			//studyPanel.SetActive(true);
			//menuOption();
		}
	}
	
	public void menuOption()
	{
		//Debug.Log("menu option trigger");
		
		if(menuActive)
		{
			firstButton.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,90,0);
			firstButtonChild.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,90,0);
			firstButtonChildTwo.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,90,0);
			commandPanel.SetActive(true);
			secondButton.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,90,0);
			thirdButton.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,90,0);
			thirdButtonChild.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,90,0);
			thirdButtonChildTwo.GetComponent<RectTransform>().rotation = Quaternion.Euler(0, 90, 0);
			mainButton.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,90,0);
			
			//timeStatsPanel.SetActive(false);
			//newsPanel.SetActive(false);
			
			menuActive = false;
			//myArrow.SetActive(false);
			//mRotation = 180F;
			//myQSet();
			//myArrow.GetComponent<RectTransform>().rotation = Quaternion.Euler(0, 0, 0);
			
			
			if(informationPanel.activeSelf)
			{
				informationPanel.SetActive(false);
				if(PlayerPrefs.GetString("Target") != "No Target")
				{
					victimPanel.SetActive(true);
				}
				else
				{
					victimPanel.SetActive(false);
					//myPanel.GetComponent<RectTransform>().rotation = Quaternion.Euler(0, 270, 0)
				}
			}
			if(PlayerPrefs.GetString("Target") == "No Target")
			{
				dataText.text = "Select a Target";
				dataText2.text = "Select a Target";
			}
			
		}
		else
		{
			firstButton.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,0,0);
			firstButtonChild.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,0,0);
			firstButtonChildTwo.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,0,0);
			commandPanel.SetActive(false);
			//timeStatsPanel.SetActive(true);
			//newsPanel.SetActive(true);
			
			//myArrow.SetActive(true);
			//mRotation = 0F;
			//myQSet();
		
			secondButton.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,0,0);
			thirdButton.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,0,0);
			thirdButtonChild.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,0,0);
			thirdButtonChildTwo.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,0,0);
			mainButton.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,0,0);
			myPanel.SetActive(false);
			menuActive = true;
		}
	}
}
