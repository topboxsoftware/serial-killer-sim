﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WorldButtoncontrol : MonoBehaviour {

	// Use this for initialization
	
	public GameObject menuButton;
    public GameObject victimButton;
    public GameObject actionButton;
    public GameObject storeButton;
    public GameObject mainPanel;
    public GameObject stalkButton;
    public GameObject studyButton;
    
    public GameObject drawerPanel1;
    public GameObject drawerPanel2;
    
    public GameObject targetButton;
    
	public GameObject mainMenu;
	public GameObject closeMenuPanel;
	public GameObject moreInfoPanel;
    
    private GameObject mailIcon;
    public Text letterContent;
	
	public Text firstOption;
	public Text secondOption;
	public Text thirdOption;
	
	public Text mainHeader;
	public Text secondaryHeader;
	
	public Text summaryText;
	public Text ageText;
	public Text jobText;
	public Text visibilityText;
	public Text friendsText;
	
	public Text firstButtonText;
	public Text secondButtonText;
    
    public GameObject timePanel;
    public Text dayText;
    public Text timeText;
	
	private bool menuOpened;
    private bool rolledOut;
    private bool switchedDrawer;
    
    private int rolledOutChild;
	private string targetContent;
	private int currentFocus;
	private float arrowRotation;
    
    private Vector2 startPos;
    
    private GameObject controlArrow;
    public Text targetInfoText;
    public Text targetNameText;
   
	//private Vector2 menuButtonPosStart;
	//private Vector2 menuButtonPosFinal;
    private Vector2 victimButtonFinalPos;
    private Vector2 actionButtonFinalPos;
    private Vector2 storeButtonFinalPos;
    private Vector2 stalkButtonFinalPos;
    
    private Vector2 targetButtonFinalPos;
    private Vector2 hidingPlace;
    
    private Vector3 firstPanel;
    private Vector3 secondPanel;
    
    private int currentTime;
    
    private string letterContentHeader;
    private string letterContentText1;
    private string letterContentText2;
    
    public Image victimImage;
    public Image victimImageCopy;
    private GameObject dayOrNight;
    
    //Boys
    private Sprite Josh;
    private Sprite Chris;
    private Sprite Eugene;
    private Sprite Ishan;
    private Sprite Jim;
    private Sprite Kartik;
    private Sprite Justin;
    private Sprite Martin;
    private Sprite Sammy;
    
    //Girls
    private Sprite Ava;
    private Sprite Erika;
    private Sprite Evelyn;
    private Sprite Fiona;
    private Sprite Kristin;
    private Sprite Lynn;
    private Sprite Molly;
    private Sprite Ruth;
    private Sprite Sarah;
    private Sprite Tammy;
    
    //Stalking Options
    private Sprite Stalker;
    private Sprite Conversation;
    private Sprite Hire;
    
    private Sprite NoAction;
    
    private int hours;
    private int minutes;
    private string amOrPm;
    private bool lastMove;
    
    
    private float shader;
   
    
	//public LRect;
	//LeanTween.scale( avatarScale, new Vector3(1.7f, 1.7f, 1.7f), 5f).setEase(LeanTweenType.easeOutBounce);
	void Start () 
	{
        //Application.targetFrameRate = 1000;
		PlayerPrefs.DeleteAll();
		PlayerPrefs.SetString("currentTarget", "No Target");
        //PlayerPrefs.SetString("LetterActive", "No");
        
        InvokeRepeating("timeData", 1F, 1F);
        InvokeRepeating("timeUpdate", 1F, .15F);
		
		menuOpened = false;
        rolledOut = false;
        switchedDrawer = false;
		targetContent = "victimSelect";
		currentFocus = 0;
        rolledOutChild = 0;
        
        arrowRotation = 0F;
        PlayerPrefs.SetInt("currentTime", 0);
        currentTime = PlayerPrefs.GetInt("currentTime");
        hours = 8;
        minutes = 0;
        lastMove = false;
        amOrPm = "am";
        
       // shade1 = new Color(0F, 0F, 0F, .33F);
        shader = 0F;
		
		//menuButtonPosStart = GameObject.Find("MenuPanel").GetComponent<Transform>().position;
		//menuButtonPosFinal = GameObject.Find("MenuButtonDestination").GetComponent<Transform>().position;
        
        victimButtonFinalPos = GameObject.Find("VictimButtonFinalPos").GetComponent<Transform>().position;
        actionButtonFinalPos = GameObject.Find("ActionButtonFinalPos").GetComponent<Transform>().position;
        storeButtonFinalPos = GameObject.Find("StoreButtonFinalPos").GetComponent<Transform>().position;
        targetButtonFinalPos = GameObject.Find("TargetButtonFinalPos").GetComponent<Transform>().position;
        hidingPlace = GameObject.Find("ButtonHidingPoint").GetComponent<Transform>().position;
        stalkButtonFinalPos = GameObject.Find("StalkButtonFinalPos").GetComponent<Transform>().position;
        
        controlArrow = GameObject.Find("ScrollDownIcon");
        
        firstPanel = drawerPanel1.GetComponent<Transform>().position;
        secondPanel = drawerPanel2.GetComponent<Transform>().position;
        
        mailIcon = GameObject.Find("MailIcon");
        
        //envelopeAppear();
        startPos = victimButton.GetComponent<Transform>().position;
            
        Josh = Resources.Load<Sprite>("Josh");
        Chris = Resources.Load<Sprite>("Chris");
        Eugene = Resources.Load<Sprite>("Eugene");
        Ishan = Resources.Load<Sprite>("Ishan");
        Jim = Resources.Load<Sprite>("Jim");
        Kartik = Resources.Load<Sprite>("Kartik");
        Justin = Resources.Load<Sprite>("Justin");
        Josh = Resources.Load<Sprite>("Josh");
        Martin = Resources.Load<Sprite>("Martin");
        Sammy = Resources.Load<Sprite>("Sammy");
        
        Ava = Resources.Load<Sprite>("Ava");
        Erika = Resources.Load<Sprite>("Erika");
        Evelyn = Resources.Load<Sprite>("Evelyn");
        Fiona = Resources.Load<Sprite>("Fiona");
        Kristin = Resources.Load<Sprite>("Kristin");
        Lynn = Resources.Load<Sprite>("Lynn");
        Molly = Resources.Load<Sprite>("Molly");
        Ruth = Resources.Load<Sprite>("Ruth");
        Sarah = Resources.Load<Sprite>("Sarah");
        Tammy = Resources.Load<Sprite>("Tammy");
        
        
        NoAction = Resources.Load<Sprite>("Unknown");
        
        Stalker = Resources.Load<Sprite>("Stalker");
        Conversation = Resources.Load<Sprite>("Conversation");
        Hire = Resources.Load<Sprite>("Hire");
        dayOrNight = GameObject.Find("Day + Night");
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
    
    void timeUpdate()
    {
        if(!menuOpened)
        {
            
            
            minutes += 10;
            
            if(minutes == 60)
             {
                 hours++;
                 minutes = 0;
             }
                
                
                if(hours == 12)
                {
                    
                    if(!lastMove)
                    {
                        if(amOrPm == "am")
                        {
                            amOrPm = "pm";
                        }
                        else if(amOrPm == "pm")
                        {
                            amOrPm = "am";
                        }
                    }
                    
                    lastMove = true;
                }
                else if(hours == 13)
                {
                    hours = 1;
                    lastMove = false;
                }
                
                
            
                
                if(minutes != 0)
                {
                    timeText.text = (hours) + ":" + minutes + amOrPm;
                }
                else
                {
                    timeText.text = (hours) + ":00" + amOrPm; 
                }
                
                //Day/Night Cycle
                
                if(amOrPm == "pm")
                {
                    shader += 0.01F;
                    dayOrNight.GetComponent<Image>().color = new Color(0F, 0F, 0F, shader);   
                }
                
                else if((hours == 3 || hours == 4 || hours == 5 || hours == 6)  && amOrPm == "am")
                {
                    Debug.Log("not active");
                    shader -= 0.03F;
                    dayOrNight.GetComponent<Image>().color = new Color(0F, 0F, 0F, shader);
                }
                
              
                
                
                /*
                if(hours == 9 && amOrPm == "am")
                {
                    Debug.Log("ye");
                    dayOrNight.GetComponent<Image>().color = shade1;
                }
                */
        }
    }
    
   
    
    void timeData()
    {
        //All time events should happen here.
    
        if(!menuOpened)
        {
            //Time assignment
            
           
            
            currentTime += 1;
            //minutes += 10;
            PlayerPrefs.SetInt("currentTime", currentTime);
            //Debug.Log(PlayerPrefs.GetInt("currentTime"));
            
        
            
            
            //Time events 
            if(currentTime == 3)
            {
                letterContentHeader = "Letter from the Shadowy Passenger";
                letterContentText1 = "I've been quiet for many years, but I won't be quiet now. It is time for us — both of us — to get to work. Go to the victim tab to get started.";
                letterContentText2 = "Right now nobody knows us, soon all will fear our name. It's just a matter of time.";
                envelopeAppear();
            }
            
            //Delete this to activate the second letter
            
            /*
            else if(currentTime == 10 && PlayerPrefs.GetString("currentTarget") != "No Target")
            {
                Debug.Log("message 2");
                letterContentHeader = "Letter from the Shadowy Passenger";
                letterContentText1 = "Excellent! It looks like you've found yourself a victim. Now it's time to get to know them so that you may eliminate them when the time is right.";
                letterContentText2 = "To get started, you can stalk them in the victim tab or research murder methods in the actions tab.";
                envelopeAppear();
            }
            */
        }
    }
    
    void  envelopeAppear()
    {
        /*
        LeanTween.scale(mailIcon, new Vector3(1F, 1F, 1F), 1.2F).setEase(LeanTweenType.easeInBounce);
        PlayerPrefs.SetString("LetterActive", "Yes");
        */
    }
    
    void envelopeDisappear()
    {
         LeanTween.scale(mailIcon, new Vector3(0F, 0F, 0F), 1.2F).setEase(LeanTweenType.easeOutBounce);
         
    }
    
    public void viewLetterContent()
    {
        envelopeDisappear();
        targetContent = "letter";
        PlayerPrefs.SetString("LetterActive", "No");
        openMenu();
    }
    
    void resetRoll()
    {
        victimButton.GetComponent<Transform>().position = startPos;
        actionButton.GetComponent<Transform>().position = startPos;
        storeButton.GetComponent<Transform>().position = startPos;
        targetButton.GetComponent<Transform>().position = startPos;
        stalkButton.GetComponent<Transform>().position = startPos;
        studyButton.GetComponent<Transform>().position = startPos;
        
        rolledOut = false;
        rolledOutChild = 0;
    }
    
    void rollOutMenu()
    {
        if(!rolledOut)
        {
            LeanTween.move(victimButton, victimButtonFinalPos, .15F).setEase(LeanTweenType.easeOutQuad);   
            LeanTween.move(actionButton, actionButtonFinalPos, .25F).setEase(LeanTweenType.easeOutQuad);
            LeanTween.move(storeButton, storeButtonFinalPos, .25F).setEase(LeanTweenType.easeOutQuad);
            LeanTween.move(targetButton, victimButtonFinalPos, .15F).setEase(LeanTweenType.easeOutQuad);
            LeanTween.move(stalkButton, actionButtonFinalPos, .25F).setEase(LeanTweenType.easeOutQuad);
            LeanTween.move(studyButton, actionButtonFinalPos, .25F).setEase(LeanTweenType.easeOutQuad);
            
            rolledOut = true;
            //rolledOutChild = 1;
        }
        else
        {
            if(rolledOutChild == 0)
            {
                rollInMenu();
            }
            else if(rolledOutChild == 1)
            {
                LeanTween.move(targetButton, victimButtonFinalPos, .15F).setEase(LeanTweenType.easeInQuad);
                LeanTween.move(stalkButton, actionButtonFinalPos, .15F).setEase(LeanTweenType.easeInQuad);
                Invoke("rollInMenu", .15F);
            }
            else if(rolledOutChild == 2)
            {
                LeanTween.move(studyButton, actionButtonFinalPos, .15F).setEase(LeanTweenType.easeInQuad);
                Invoke("rollInMenu", .15F);
            }
        }
    }
    
    void rollOutVictim()
    {
        
        if(rolledOutChild == 0)
        {
            LeanTween.move(targetButton, targetButtonFinalPos, .3F).setEase(LeanTweenType.easeOutQuad);
            LeanTween.move(stalkButton, stalkButtonFinalPos, .3F).setEase(LeanTweenType.easeOutQuad);
            
            rolledOutChild = 1;
        }
        else if(rolledOutChild == 1)
        {
            //Roll in victim buttons, set rolledOutChild back to 0.
            LeanTween.move(targetButton, victimButtonFinalPos, .3F).setEase(LeanTweenType.easeInQuad);
            LeanTween.move(stalkButton, actionButtonFinalPos, .3F).setEase(LeanTweenType.easeInQuad);
            
            rolledOutChild = 0;
        }
        else if(rolledOutChild == 2)
        {
            LeanTween.move(studyButton, actionButtonFinalPos, .15F).setEase(LeanTweenType.easeOutQuad);
            LeanTween.move(targetButton, targetButtonFinalPos, .3F).setEase(LeanTweenType.easeOutQuad);
            LeanTween.move(stalkButton, stalkButtonFinalPos, .3F).setEase(LeanTweenType.easeOutQuad);
            
            rolledOutChild = 1;
        }
       
        
    
    }
    
    void rollOutAction()
    {
        
        if(rolledOutChild == 0)
        {
            LeanTween.move(studyButton, stalkButtonFinalPos, .3F).setEase(LeanTweenType.easeOutQuad);
            
            rolledOutChild = 2;
        }
        else if(rolledOutChild == 1)
        {
           
            LeanTween.move(targetButton, victimButtonFinalPos, .3F).setEase(LeanTweenType.easeInQuad);
            LeanTween.move(stalkButton, actionButtonFinalPos, .3F).setEase(LeanTweenType.easeInQuad);
            LeanTween.move(studyButton, stalkButtonFinalPos, .3F).setEase(LeanTweenType.easeOutQuad);
            
           rolledOutChild = 2;
        }
        else if(rolledOutChild == 2)
        {
            //Roll in action buttons, set rolledOutChild back to 0.
            LeanTween.move(studyButton, actionButtonFinalPos, .3F).setEase(LeanTweenType.easeInQuad);
            
            rolledOutChild = 0;
        }
       
        
    
    }
    
    void rollInMenu()
    {
            LeanTween.move(victimButton, startPos, .15F).setEase(LeanTweenType.easeInQuad);   
            LeanTween.move(actionButton, startPos, .25F).setEase(LeanTweenType.easeInQuad);
            LeanTween.move(storeButton, startPos, .35F).setEase(LeanTweenType.easeInQuad);
            LeanTween.move(targetButton, startPos, .15F).setEase(LeanTweenType.easeInQuad);
            LeanTween.move(stalkButton, startPos, .25F).setEase(LeanTweenType.easeInQuad);
            LeanTween.move(studyButton, startPos, .25F).setEase(LeanTweenType.easeInQuad);
            
            rolledOut = false;
            rolledOutChild = 0; 
    }
    
    
  
	void generateVictimData()
	{
		//Generating Names
		newName("FirstName1", "LastName1");
		newName("FirstName2", "LastName2");
		newName("FirstName3", "LastName3");
		
		//Generating Ages
		newAge("Age1");
		newAge("Age2");
		newAge("Age3");
		
		//Generating Jobs
		newJob("Job1");
		newJob("Job2");
		newJob("Job3");
		
		//Generating Visibility
		newVisibility("Visibility1");
		newVisibility("Visibility2");
		newVisibility("Visibility3");

		//Generating Friends		
		newFriends("Friends1");
		newFriends("Friends2");
		newFriends("Friends3");
	}
	
	void newFriends(string friends)
	{
		//Consider typing certain friend counts to job + age
		int x = (int)Random.Range(0F, 10F);
		PlayerPrefs.SetInt(friends, x);
	}
	
	void newName(string first, string  last)
	{
		//Generates and assigns a new name
		int x = (int)Random.Range(1F, 19F);
		int y = (int)Random.Range(1F, 10F);
		
		if(x == 1)
		{
			PlayerPrefs.SetString(first, "Josh");
		}
		else if(x == 2)
		{
			PlayerPrefs.SetString(first, "Sammy");
		}
		else if(x == 3)
		{
			PlayerPrefs.SetString(first, "Tammy");
		}
		else if(x == 4)
		{
			PlayerPrefs.SetString(first, "Sarah");
		}
		else if(x == 5)
		{
			PlayerPrefs.SetString(first, "Chris");
		}
		else if(x == 6)
		{
			PlayerPrefs.SetString(first, "Fiona");
		}
		else if(x == 7)
		{
			PlayerPrefs.SetString(first, "Jim");
		}
		else if(x == 8)
		{
			PlayerPrefs.SetString(first, "Molly");
		}
		else if(x == 9)
		{
			PlayerPrefs.SetString(first, "Kristin");
		}
		else if(x == 10)
		{
			PlayerPrefs.SetString(first, "Erika");
		}
        else if(x == 11)
        {
            PlayerPrefs.SetString(first, "Eugene");
        }
        else if(x == 12)
        {
            PlayerPrefs.SetString(first, "Ishan");
        }
        else if(x == 13)
        {
            PlayerPrefs.SetString(first, "Justin");
        }
        else if(x == 14)
        {
            PlayerPrefs.SetString(first, "Kartik");
        }
        else if(x == 15)
        {
            PlayerPrefs.SetString(first, "Martin");
        }
        else if(x == 16)
        {
            PlayerPrefs.SetString(first, "Lynn");
        }
		else if(x == 17)
        {
            PlayerPrefs.SetString(first, "Evelyn");
        }
        else if(x == 18)
        {
            PlayerPrefs.SetString(first, "Ruth");
        }
        else if(x == 19)
        {
            PlayerPrefs.SetString(first, "Ava");
        }
      
        
		if(y == 1)
		{
			PlayerPrefs.SetString(last, "Williams");
		}
		else if(y == 2)
		{
			PlayerPrefs.SetString(last, "Jones");
		}
		else if(y == 3)
		{
			PlayerPrefs.SetString(last, "Thomas");
		}
		else if(y == 4)
		{
			PlayerPrefs.SetString(last, "Wilson");
		}
		else if(y == 5)
		{
			PlayerPrefs.SetString(last, "Kim");
		}
		else if(y == 6)
		{
			PlayerPrefs.SetString(last, "Lee");
		}
		else if(y == 7)
		{
			PlayerPrefs.SetString(last, "Smith");
		}
		else if(y == 8)
		{
			PlayerPrefs.SetString(last, "Jordan");
		}
		else if(y == 9)
		{
			PlayerPrefs.SetString(last, "Taparia");
		}
		else if(y == 10)
		{
			PlayerPrefs.SetString(last, "Wang");
		}
	}
	
	void newAge(string age)
	{
		int x = (int)Random.Range(16F, 85F);
		PlayerPrefs.SetInt(age, x);
	}
	
	void newVisibility(string visibility)
	{
		//Considering tying certain visibilities jobs
		int x = (int)Random.Range(1F, 10F);
		PlayerPrefs.SetInt(visibility, x);
	}
	
	void newJob(string job)
	{
		int x = (int)Random.Range(1F, 11F);
		
		if(x == 1)
		{
			PlayerPrefs.SetString(job, "Doctor");
		}
		else if(x == 2)
		{
			PlayerPrefs.SetString(job, "Author");
		}
		else if(x == 3)
		{
			PlayerPrefs.SetString(job, "Unemployed");
		}
		else if(x == 4)
		{
			PlayerPrefs.SetString(job, "Teacher");
		}
		else if(x == 5)
		{
			PlayerPrefs.SetString(job, "Counselor");
		}
		else if(x == 6)
		{
			PlayerPrefs.SetString(job, "Farmer");
		}
		else if(x ==7)
		{
			PlayerPrefs.SetString(job, "Banker");
		}
		else if(x == 8)
		{
			PlayerPrefs.SetString(job, "Clerk");
		}
		else if(x == 9)
		{
			PlayerPrefs.SetString(job, "Janitor");
		}
		else if(x == 10)
		{
			PlayerPrefs.SetString(job, "Chef");
		}
		else if(x ==11)
		{
			PlayerPrefs.SetString(job, "Hitman");
		}
	}
	
	public void openMenu()
	{
        if(switchedDrawer)
        {
            switchDrawer();    
        }
        
		if(PlayerPrefs.GetString("MenuOpenedEver") != "Yes")
		{
			PlayerPrefs.SetString("MenuOpenedEver", "Yes");
			generateVictimData();
		}
		
		if (!menuOpened)
		{
			//LeanTween.moveX( gameObject, 1f, 1f).setEase( LeanTweenType.easeInQuad ).setDelay(1f);
			//LeanTween.scale(menuButton, new Vector3(0F, 0F, 0F), .3F).setEase(LeanTweenType.linear);
			//LeanTween.move(menuButton, new Vector2(menuButton.GetComponent<Transform>().position.x, menuButton.GetComponent<Transform>().position.y + 100F)).setEase(LeanTweenType.easeInQuad);
			
			//LeanTween.move(menuButton, menuButtonPosFinal, .3F).setEase(LeanTweenType.easeInQuad);
			//rollInMenu();
            
            //This code will be called when the menu is opened.
            
			LeanTween.scale(mainMenu, new Vector3(1F, 1F, 1F), .3F).setEase(LeanTweenType.easeOutBack);
		    
            
             
                
            if(PlayerPrefs.GetString("LetterActive") == "Yes")
            {
                envelopeDisappear();
            }
            
			closeMenuPanel.SetActive(true);
			menuOpened = true;	
			
            mainPanel.SetActive(false);
            timePanel.SetActive(false);
            
            
            resetRoll();
            
            if(letterContent.text != "")
            {
                letterContent.text = "";
            }
            
			if(PlayerPrefs.GetString("currentTarget") != "No Target")
			{
                if(targetContent != "letter" && targetContent != "stalkingVictim")
                {
                    if(PlayerPrefs.GetString("currentTarget") == "first")
                    {
                        mainHeader.text = PlayerPrefs.GetString("FirstName1") + " " + PlayerPrefs.GetString("LastName1");
                
                    }
                    else if(PlayerPrefs.GetString("currentTarget") == "second")
                    {
                        mainHeader.text = PlayerPrefs.GetString("FirstName2") + " " + PlayerPrefs.GetString("LastName2");
                        
                    }
                    else if(PlayerPrefs.GetString("currentTarget") == "third")
                    {
                        mainHeader.text = PlayerPrefs.GetString("FirstName3") + " " + PlayerPrefs.GetString("LastName3");
                    }
                }
                else
                {
                    mainHeader.text = "Letter";
                    
                    summaryText.text = letterContentHeader;
                    ageText.text = "";
                    jobText.text = "";
                    visibilityText.text = "";
                    friendsText.text = "";
                    letterContent.text = letterContentText1;
                    
                    firstButtonText.text = "Next Page";
                    secondButtonText.text = "Close Letter";
                }
				
				

				//Fill this information in with data
                if(targetContent != "victimInformation" && targetContent != "letter" && targetContent != "stalkingVictim")
                {
                    targetContent = "killOrGiveUp";
                    
                    summaryText.text = "Mindset - Completely oblivious";	
                    ageText.text = "Research -  0"; 
                    jobText.text = "Money - $0";
                    visibilityText.text = "Weapons - 0";
                    friendsText.text = "Police awareness - 0";
                    
                    firstButtonText.text = "Kill Attempt";
                    secondButtonText.text = "Give Up";
                    
                    
                }
                else
                {
                    updateMenuData();
                }
                
               
				
				//This will happen no matter who the target is 
				LeanTween.scale(moreInfoPanel, new Vector3(1F, 1F, 1F), 0F);
			}
			else
			{
				mainHeader.text = "Target Select";
			}
            
            if(targetContent == "letter")
            {
                LeanTween.scale(moreInfoPanel, new Vector3(1F, 1F, 1F), 0F);
                mainHeader.text = "Letter";
                
                summaryText.text = letterContentHeader;
                ageText.text = "";
                jobText.text = "";
                visibilityText.text = "";
                friendsText.text = "";
                letterContent.text = letterContentText1;
                
                firstButtonText.text = "Next Page";
                secondButtonText.text = "Close Letter";
            }
			
		}
		else
		{
			//LeanTween.move(menuButton, menuButtonPosStart, .3F).setEase(LeanTweenType.easeOutQuad);
            
            //This code will be called when the menu is closed.
            
           
           
            mainPanel.SetActive(true);
            Invoke("enableTimePanel", .3F);
            resetRoll();
			LeanTween.scale(mainMenu, new Vector3(0F, 0F, 0F), .3F).setEase(LeanTweenType.easeInBack);				
			LeanTween.scale(moreInfoPanel, new Vector3(0F, 0F, 0F), .3F).setEase(LeanTweenType.easeInBack);	
			
			currentFocus = 0;
            
            victimImage.sprite = NoAction;
            
            if(targetContent == "stalkingVictim")
            {
		      Invoke("resetTargetContent", 0.3F);
            }
            else
            {
                targetContent = "victimSelect";
            }
            
			secondaryHeader.text = "Select a Victim";
            
            if(PlayerPrefs.GetString("LetterActive") == "Yes")
            {
                envelopeAppear();
            }
			
			if(PlayerPrefs.GetString("Target") == "No Target")
			{
				mainHeader.text = "Target Select";
			}
			
			closeMenuPanel.SetActive(false);
			menuOpened = false;
		}
		
		if(targetContent == "victimSelect")
		{
			firstOption.text = PlayerPrefs.GetString("FirstName1") + " " + PlayerPrefs.GetString("LastName1");
			secondOption.text = PlayerPrefs.GetString("FirstName2") + " " + PlayerPrefs.GetString("LastName2");
			thirdOption.text = PlayerPrefs.GetString("FirstName3") + " " + PlayerPrefs.GetString("LastName3");
		}
       
	}
    
    void enableTimePanel()
    {
        timePanel.SetActive(true);
    }
    
    void resetTargetContent()
    {
        targetContent = "victimSelect";
    }
	
	public void optionAssign(string optionSelected)
	{
		//Identify what content is being selected 
		if(targetContent == "victimSelect")	
		{
            //PlayerPrefs.SetString("FirstName1", "Josh");
            //PlayerPrefs.SetString("FirstName2", "Chris");
            
			if(optionSelected == "optionOne")
			{
				secondaryHeader.text = PlayerPrefs.GetString("FirstName1") + " " + PlayerPrefs.GetString("LastName1");
				currentFocus = 1;
			}
			else if(optionSelected == "optionTwo")
			{
				secondaryHeader.text = PlayerPrefs.GetString("FirstName2") + " " + PlayerPrefs.GetString("LastName2");
				currentFocus = 2;
			}
			else if(optionSelected == "optionThree")
			{
				secondaryHeader.text = PlayerPrefs.GetString("FirstName3") + " " + PlayerPrefs.GetString("LastName3");
				currentFocus = 3;
			}
            
            if(secondaryHeader.text.Contains("Josh"))
            {
                victimImage.sprite = Josh;  
                victimImageCopy.sprite = Josh;
            }
            else if(secondaryHeader.text.Contains("Chris"))
            {
               victimImage.sprite = Chris;
               victimImageCopy.sprite = Chris;
            }
            else if(secondaryHeader.text.Contains("Eugene"))
            {
               victimImage.sprite = Eugene;
               victimImageCopy.sprite = Eugene;
            }
            else if(secondaryHeader.text.Contains("Ishan"))
            {
               victimImage.sprite = Ishan;
               victimImageCopy.sprite = Ishan;
            }
            else if(secondaryHeader.text.Contains("Jim"))
            {
               victimImage.sprite = Jim;
               victimImageCopy.sprite = Jim;
            }
            else if(secondaryHeader.text.Contains("Kartik"))
            {
               victimImage.sprite = Kartik;
               victimImageCopy.sprite = Kartik;
            }
            else if(secondaryHeader.text.Contains("Justin"))
            {
               victimImage.sprite = Justin;
               victimImageCopy.sprite = Justin;
            }
            else if(secondaryHeader.text.Contains("Martin"))
            {
               victimImage.sprite = Martin;
               victimImageCopy.sprite = Martin;
            }
            else if(secondaryHeader.text.Contains("Sammy"))
            {
               victimImage.sprite = Sammy;
               victimImageCopy.sprite = Sammy;
            }
            else if(secondaryHeader.text.Contains("Ava"))
            {
                victimImage.sprite = Ava;
                victimImageCopy.sprite = Ava;   
            }
            else if(secondaryHeader.text.Contains("Erika"))
            {
               victimImage.sprite = Erika;
               victimImageCopy.sprite = Erika;
            }
            else if(secondaryHeader.text.Contains("Evelyn"))
            {
               victimImage.sprite = Evelyn;
               victimImageCopy.sprite = Evelyn;
            }
            else if(secondaryHeader.text.Contains("Fiona"))
            {
               victimImage.sprite = Fiona;
               victimImageCopy.sprite = Fiona;
            }
            else if(secondaryHeader.text.Contains("Kristin"))
            {
               victimImage.sprite = Kristin;
               victimImageCopy.sprite = Kristin;
            }
            else if(secondaryHeader.text.Contains("Lynn"))
            {
               victimImage.sprite = Lynn;
               victimImageCopy.sprite = Lynn;
            }
            else if(secondaryHeader.text.Contains("Molly"))
            {
               victimImage.sprite = Molly;
               victimImageCopy.sprite = Molly;
            }
            else if(secondaryHeader.text.Contains("Ruth"))
            {
               victimImage.sprite = Ruth;
               victimImageCopy.sprite = Ruth;
            }
            else if(secondaryHeader.text.Contains("Sarah"))
            {
               victimImage.sprite = Sarah;
               victimImageCopy.sprite = Sarah;
            }
            else if(secondaryHeader.text.Contains("Tammy"))
            {
               victimImage.sprite = Tammy;
               victimImageCopy.sprite = Tammy;
            }
           
            
		}
        else if(targetContent == "stalkingVictim")
        {
            Debug.Log("wow");
            
            if(optionSelected == "optionOne")
            {
                currentFocus = 1;
                secondaryHeader.text = "Follow Around";
            }
            else if(optionSelected == "optionTwo")
            {
                currentFocus = 2;
                secondaryHeader.text = "Talk to Victim";
            }
            else if(optionSelected == "optionThree")
            {
                currentFocus = 3;
                secondaryHeader.text = "Hire Private Eye";
            }
            
            if(secondaryHeader.text.Contains("Follow"))
            {
                victimImage.sprite = Stalker;
            }
            else if(secondaryHeader.text.Contains("Talk"))
            {
                victimImage.sprite = Conversation;
            }
            else if(secondaryHeader.text.Contains("Hire"))
            {
                victimImage.sprite = Hire;
            }
            
            
            
        }	
	}
    
    void updateMenuData()
    {
        if(targetContent == "victimInformation")
        {
            LeanTween.scale(moreInfoPanel, new Vector3(1F, 1F, 1F), .3F).setEase(LeanTweenType.easeOutBack);
				
				if(PlayerPrefs.GetString("currentTarget") == "first")
				{
					mainHeader.text = PlayerPrefs.GetString("FirstName1") + " " + PlayerPrefs.GetString("LastName1");
					
					summaryText.text = "Summary - Loner with few friends";
					ageText.text = "Age - " + PlayerPrefs.GetInt("Age1");
					jobText.text = "Job - " + PlayerPrefs.GetString("Job1");
					visibilityText.text = "Visibility - " + PlayerPrefs.GetInt("Visibility1");
					friendsText.text = "Friends - " + PlayerPrefs.GetInt("Friends1");	
				}
				else if(PlayerPrefs.GetString("currentTarget") == "second")
				{
					mainHeader.text = PlayerPrefs.GetString("FirstName2") + " " + PlayerPrefs.GetString("LastName2");
					
					summaryText.text = "Summary - Loner with few friends";
					ageText.text = "Age - " + PlayerPrefs.GetInt("Age2");
					jobText.text = "Job - " + PlayerPrefs.GetString("Job2");
					visibilityText.text = "Visibility - " + PlayerPrefs.GetInt("Visibility2");
					friendsText.text = "Friends - " + PlayerPrefs.GetInt("Friends2");
				}
				else if(PlayerPrefs.GetString("currentTarget") == "third")
				{
					mainHeader.text = PlayerPrefs.GetString("FirstName3") + " " + PlayerPrefs.GetString("LastName3");
					
					summaryText.text = "Summary - Loner with few friends";
					ageText.text = "Age - " + PlayerPrefs.GetInt("Age3");
					jobText.text = "Job - " + PlayerPrefs.GetString("Job3");
					visibilityText.text = "Visibility - " + PlayerPrefs.GetInt("Visibility3");
					friendsText.text = "Friends - " + PlayerPrefs.GetInt("Friends3");
				}
        }
        
        firstButtonText.text = "Kill Attempt";
		secondButtonText.text = "Give Up";
    }
	
	public void updateMenu()
	{
		if(currentFocus != 0)
		{
			if(targetContent == "victimSelect")
			{
				
				targetContent = "victimInfo";
				
				LeanTween.scale(moreInfoPanel, new Vector3(1F, 1F, 1F), .3F).setEase(LeanTweenType.easeOutBack);
				
				if(currentFocus == 1)
				{
					mainHeader.text = PlayerPrefs.GetString("FirstName1") + " " + PlayerPrefs.GetString("LastName1");
					
					summaryText.text = "Summary - Loner with few friends";
					ageText.text = "Age - " + PlayerPrefs.GetInt("Age1");
					jobText.text = "Job - " + PlayerPrefs.GetString("Job1");
					visibilityText.text = "Visibility - " + PlayerPrefs.GetInt("Visibility1");
					friendsText.text = "Friends - " + PlayerPrefs.GetInt("Friends1");	
				}
				else if(currentFocus == 2)
				{
					mainHeader.text = PlayerPrefs.GetString("FirstName2") + " " + PlayerPrefs.GetString("LastName2");
					
					summaryText.text = "Summary - Loner with few friends";
					ageText.text = "Age - " + PlayerPrefs.GetInt("Age2");
					jobText.text = "Job - " + PlayerPrefs.GetString("Job2");
					visibilityText.text = "Visibility - " + PlayerPrefs.GetInt("Visibility2");
					friendsText.text = "Friends - " + PlayerPrefs.GetInt("Friends2");
				}
				else if(currentFocus == 3)
				{
					mainHeader.text = PlayerPrefs.GetString("FirstName3") + " " + PlayerPrefs.GetString("LastName3");
					
					summaryText.text = "Summary - Loner with few friends";
					ageText.text = "Age - " + PlayerPrefs.GetInt("Age3");
					jobText.text = "Job - " + PlayerPrefs.GetString("Job3");
					visibilityText.text = "Visibility - " + PlayerPrefs.GetInt("Visibility3");
					friendsText.text = "Friends - " + PlayerPrefs.GetInt("Friends3");
				}
				
				firstButtonText.text = "Select Target";
				secondButtonText.text = "Go Back";
			}
            else if(targetContent == "stalkingVictim")
            {
                if(currentFocus == 1)
                {
                    
                }
            }   
		}
	}
	
	public void goBack()
	{
		if(targetContent == "victimInfo")
		{
			LeanTween.scale(moreInfoPanel, new Vector3(0F, 0F, 0F), .3F).setEase(LeanTweenType.easeInBack);
			currentFocus = 0;
			mainHeader.text = "Target Select";
			secondaryHeader.text = "Select a Victim";
			targetContent = "victimSelect";
		}
		
		if(targetContent == "killOrGiveUp" || targetContent == "victimInformation")
		{
			LeanTween.scale(moreInfoPanel, new Vector3(0F, 0F, 0F), .3F).setEase(LeanTweenType.easeInBack);
			currentFocus = 0;
			PlayerPrefs.SetString("currentTarget", "No Target");
			targetContent = "victimSelect";
			openMenu();
		}
        
        if(targetContent == "letter")
        {
            LeanTween.scale(moreInfoPanel, new Vector3(0F, 0F, 0F), .3F).setEase(LeanTweenType.easeInBack);
            openMenu();
        }
	}
    
    public void stalkVictim()
    {
        if(PlayerPrefs.GetString("currentTarget") != "No Target")
        {
            targetContent = "stalkingVictim";
            openMenu();
            
            //Set up stalking conditions
            
            mainHeader.text = "Stalk Victim";
            LeanTween.scale(moreInfoPanel, new Vector3(0F, 0F, 0F), 0F);
          
            firstOption.text = "Follow Around";
            secondOption.text = "Talk to Victim";
            thirdOption.text = "Hire Private Eye";
            
            Debug.Log(targetContent);
        }
    }
	
	public void victimAction()
	{
		//Either select or act upon victim
		
		if(targetContent == "victimInfo")
		{
			if(currentFocus == 1)
			{
				PlayerPrefs.SetString("currentTarget", "first");
			}
			else if(currentFocus == 2)
			{
				PlayerPrefs.SetString("currentTarget", "second");
			}
			else if(currentFocus == 3)
			{
				PlayerPrefs.SetString("currentTarget", "third");
			}
			
			//This will happen no matter who the target is
			openMenu();
		}
		else if(targetContent == "killOrGiveUp")
		{
			Debug.Log("Kill Attempt");
            //InvokeRepeating("fadeToBlack", .15F);
            //Invoke("MurderScene", 1F);
		}
        else if(targetContent == "letter")
        {
            if(letterContentText2 != "")
            {
                letterContent.text = letterContentText2;
                letterContentText2 = "";
            }
            else
            {
                letterContent.text = "No pages left.";
            }
        }
	}
    
    void fadeToBlack()
    {
      //  shader += .3F;
       // dayOrNight.GetComponent<Image>().color = new Color(0F, 0F, 0F, shader);
    }
    
    public void switchDrawer()
    {
        if(!menuOpened)
        {
            if(arrowRotation == 0F)
            {
                arrowRotation += 180F;
            }
            else
            {
                arrowRotation -= 180F;
            }
            
            if(!switchedDrawer)
            {
                
                Debug.Log(PlayerPrefs.GetString("currentTarget"));
                
                if(PlayerPrefs.GetString("currentTarget") == "No Target")
                {
                    targetNameText.text = "No current target";
                    targetInfoText.text = "Select Target";
                }
                else
                {
                    if(PlayerPrefs.GetString("currentTarget") == "first")
                    {
                        targetNameText.text = PlayerPrefs.GetString("FirstName1") + " " + PlayerPrefs.GetString("LastName1");
                    }
                    else if(PlayerPrefs.GetString("currentTarget") == "second")
                    {
                        targetNameText.text = PlayerPrefs.GetString("FirstName2") + " " + PlayerPrefs.GetString("LastName2");
                    }
                    else if(PlayerPrefs.GetString("currentTarget") == "third")
                    {
                        targetNameText.text = PlayerPrefs.GetString("FirstName3") + " " + PlayerPrefs.GetString("LastName3");
                    }
                   
                    targetInfoText.text = "More Information";
                }
                
                LeanTween.move(drawerPanel1, secondPanel, .25F).setEase(LeanTweenType.linear);
                LeanTween.move(drawerPanel2, firstPanel, .25F).setEase(LeanTweenType.linear);
                
                
                
                LeanTween.rotate(controlArrow, new Vector3(0, 0, arrowRotation), .25F);
            }
            else
            {
                LeanTween.move(drawerPanel1, firstPanel, .25F).setEase(LeanTweenType.linear);
                LeanTween.move(drawerPanel2, secondPanel, .25F).setEase(LeanTweenType.linear);
                
                LeanTween.rotate(controlArrow, new Vector3(0, 0, arrowRotation), .25F);
            }
            
            //Reverses state of switched drawer
            switchedDrawer = !switchedDrawer;
        }
    }
    
    public void targetStats()
    {
        if(PlayerPrefs.GetString("currentTarget") != "No Target")
        {
            targetContent = "victimInformation";
           
        }
        else
        {
            targetContent = "victimSelect";
        }
        
        openMenu();
       
    }
    
 
 
}
