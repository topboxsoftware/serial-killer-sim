﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NewDayScript : MonoBehaviour {

    public GameObject locationText;
    public GameObject dayText;
    public GameObject populationText;
    
    public GameObject startButton;
   
    //public Text dayText;
    
    
	// Use this for initialization
	void Start () 
    {
        fadeText();
        //LeanTween.alpha(locationText.GetComponent<RectTransform>(), 1F, 1F).setDelay(.5F);
        
	   //LeanTween.alpha(gameObject.GetComponent<RectTransform>(), 0.5f, 1f).setDelay(1f);
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
    
    void fadeText()
    {
        LeanTween.scale(locationText, new Vector3(1F, 1F, 1F), 1F).setEase(LeanTweenType.easeOutBack);
        LeanTween.scale(populationText, new Vector3(1F, 1F, 1F), 1F).setEase(LeanTweenType.easeOutBack).setDelay(1F);
        LeanTween.scale(dayText, new Vector3(1F, 1F, 1F), 1F).setEase(LeanTweenType.easeOutBack).setDelay(2F);
        
        LeanTween.scale(startButton, new Vector3(1F, 1F, 1F), 1F).setEase(LeanTweenType.easeOutBack).setDelay(3F);
        
    }
    
    public void startDay()
    {
        LeanTween.scale(gameObject, new Vector3(0F, 0F, 0F), .4F).setEase(LeanTweenType.easeInBack);
    }
}
