﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScreenFade : MonoBehaviour {

	// Creating the overlay panel and destination color
	public Image myPanel;
	Color myColor = new Color(0, 0, 0, 255);
	
	void Start ()
	{
		//fadeOut();
	}
	
	// Update is called once per frame
	void Update () 
	{
	//	myPanel.GetComponent<Image>().SetActive(false);
		
		
	}
	
	public void fadeOut()
	{
		//Debug.Log(PlayerPrefs.GetString("Testing"));
		myPanel.transform.parent = GameObject.Find("PanelDestination").transform;
		myPanel.GetComponent<Image>().CrossFadeColor(myColor, 2.0f, false, true);	
	}
}
