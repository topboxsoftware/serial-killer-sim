﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VictimButtonBehavior : MonoBehaviour {



	bool myAction;
	public Button firstButton;

	float weight;
	float speed;

	Vector3 finishFirstButton;
	Vector3 startFirstButton;

	// Use this for initialization
	void Start () 
	{

		weight = 0;
		speed = 5F;

		myAction = false;
		startFirstButton = new Vector3 (firstButton.transform.position.x, firstButton.transform.position.y, firstButton.transform.position.z);
		finishFirstButton = new Vector3 (startFirstButton.x + firstButton.GetComponent<RectTransform>().rect.width * .68F, startFirstButton.y, startFirstButton.z);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (myAction) 
		{

				weight = weight + Time.deltaTime * speed;
				
				firstButton.transform.position = Vector3.Lerp (startFirstButton, finishFirstButton, weight);

				

		}
	}

	public void onClick()
	{
		if (!myAction)
		{
			myAction = true;
		}
	}
	        
}
