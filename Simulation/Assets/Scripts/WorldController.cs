﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WorldController : MonoBehaviour {

	// Use this for initialization

	private Text weekText;
	private Text dayText;

	int timeCount;

	int weeks;
	int days;
	
	bool textFlow; 
	
	public Text newsText;
	float newsPanelX;
	
	int currentMessage;
	
	Vector2 newsPosition;
	Vector2 newsStart;
	int x;
	
	string[] newsMessages = {"zach" ,"cat"};
	void Start () 
	{

		timeCount = 0;
		weeks = 1;
		days = 1;
		currentMessage = 1;

		weekText = GameObject.Find ("WeekCounter").GetComponent<Text> ();
		dayText = GameObject.Find ("DayCounter").GetComponent<Text> ();
		InvokeRepeating ("countTime", 1F, 1F);
		
		newsStart = GameObject.Find("NewsText").GetComponent<Transform>().position;
		newsPanelX = GameObject.Find("NewsPanel").GetComponent<RectTransform>().rect.width;
		textFlow = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(textFlow)
		{
			
			if(newsPosition.x > newsPanelX)
			{
				genNewMessage();
				newsPosition = new Vector2(newsStart.x, newsStart.y);
				newsText.GetComponent<RectTransform>().position = newsPosition;
			}
			
			newsPosition = new Vector2(newsText.GetComponent<RectTransform>().position.x + 1, newsText.GetComponent<RectTransform>().position.y);
			newsText.GetComponent<RectTransform>().position = newsPosition;
			
		}

	}
	
	void genNewMessage()
	{
		
	
		
	}

	void countTime()
	{
		timeCount++;

		//This trigger should be set to 10 seconds before distribution
		//Consider adding some sort of animation to make this look better

		if (timeCount == 1) 
		{
			days++;
			timeCount = 0;

			dayText.text = "Day " + days.ToString();
		}

		if (days > 7) 
		{
			days = 1;
			weeks++;

			dayText.text = "Day " + days.ToString();
			weekText.text = "Week " + weeks.ToString();
		}

	}
}
