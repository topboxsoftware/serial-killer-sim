﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WorldLogic : MonoBehaviour {

    public GameObject newsText;
    private Vector2 newsTextPosition;
    
    private float startNewsPoint;
    private float cutOff;
    
    private string[] myStoryList = {"story1", "story2", "story3"};
    private string myStory;
    private float rand;
    
    private float arraySize;
    
	// Use this for initialization
	void Start () 
    {
	   newsTextPosition = newsText.GetComponent<Transform>().position;
       arraySize = 11F;
       
       startNewsPoint = newsText.GetComponent<Transform>().position.x;
       cutOff = GameObject.Find("ResetPanel").GetComponent<Transform>().position.x;
	}
	
	// Update is called once per frame
	void Update ()
    {
	   newsInformation();
	}
    
    void newsInformation()
    {
        newsTextPosition = new Vector2(newsTextPosition.x - .7F, newsTextPosition.y);
       
        newsText.GetComponent<Transform>().position = newsTextPosition;
        
        if (newsTextPosition.x < cutOff)
        {
            newsTextPosition = new Vector2(startNewsPoint, newsTextPosition.y);
            newStory();
            newsText.GetComponent<Text>().text = myStory;
        }
    }
    
    void newStory()
    {
        //Random number between 1 and 10
        rand = (int)Random.Range(0F, 10F);
       
    }
}
